#!/usr/bin/env bash


docker build -t pstruschka/pdf-to-txt:extractor https://gitlab.com/pstruschka/pdf-to-text-extractor-worker.git
docker build -t pstruschka/pdf-to-txt:worker https://gitlab.com/pstruschka/pdf-to-text-service-worker.git 
docker build -t pstruschka/pdf-to-txt:compressor https://gitlab.com/pstruschka/pdf-to-text-compressor-worker.git

docker build -t pstruschka/pdf-to-txt:backend https://gitlab.com/ice48623/pdf_to_text_webapi.git
docker build -t pstruschka/pdf-to-txt:frontend https://gitlab.com/ice48623/pdf_to_text_frontend.git