#!/usr/bin/env bash

PIECES="rabbit redis minio extractor-worker pdf-to-text-worker compressor-worker web-api frontend nginx"

COMMAND="create"
FROM="--from-file=default.conf --from-file=nginx.conf"

if ! type "kubectl" > /dev/null; then
	echo "Requires kubectl in PATH"
	exit 1
fi

while getopts :d opt; do
  case ${opt} in
    d )	COMMAND="delete" 
		FROM="" ;;
    ? ) echo "Usage: $0 [-d]"
		exit 1 ;;
  esac
done

kubectl $COMMAND configmap nginx-config $FROM

for PIECE in $PIECES; do
	for CONFIG in $(find ./pieces/$PIECE/*.yaml 2> /dev/null); do
		kubectl $COMMAND -f $CONFIG
	done
done
